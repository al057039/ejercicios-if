
package ejercicios.pkgif;

import java.util.Scanner;

public class EjerciciosIf {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       // Información
       System.out.println("ID 57039 Jessica Johana Estefanía Chin Pech");
       //Menú
       int num= 0; 
       //Pedir número para el menú
       Scanner sc=new Scanner (System.in);
       //opciones del menú
       System.out.println("1.Vocales");
       System.out.println("2.Descuento");
       System.out.println("--------------------------------------------------");
       System.out.println("Ingrese un número del 1 al 2 para desplegar la información que requiere");
       num = sc.nextInt();
       //información que sale dependiendo del número
       if (num==1){
         char vocal;
       Scanner lector= new Scanner(System.in);
       System.out.println("Ingrese una vocal");
       vocal = lector.next().charAt(0);
       //Vocales
       if (vocal =='a'||vocal =='e'||vocal =='i'||vocal =='o'||vocal =='u'){
           System.out.println("La vocal ingresada es minúscula");  
       } else if (vocal =='A'||vocal =='E'||vocal =='I'||vocal =='O'||vocal =='U'){
           System.out.println("La vocal ingresada es mayúscula");
       } else {
           System.out.println("No es una vocal");
       }
       System.out.println("--------------------------------------------------");
       }else if (num==2) {
       Scanner lector = new Scanner (System.in);
       int cantidad, precio,cobrar, compra, descuento;
       //Descuento
       System.out.println("Introduzca la cantidad del producto");
       cantidad = lector.nextInt();
       System.out.println("Introduzca el precio del producto");
        precio = lector.nextInt();
        compra= precio*cantidad;
        if (compra>=50){
            cobrar= (int) (compra-compra*0.1);
            System.out.println("Se le cobrará " + "$"+cobrar);
        } else {
            cobrar= (int) (compra-compra*0.08);
            System.out.println("Se le cobrará " + "$"+cobrar);
        }
        if (compra>=50){
            descuento= (int) (compra*0.1);
            System.out.println("y se le descontará " + "$"+descuento);  
        } else {
            descuento= (int) (compra*0.08);
            System.out.println("y se le descontará " + "$"+descuento);
        } 
       System.out.println("--------------------------------------------------");
        }
    }
    
}
